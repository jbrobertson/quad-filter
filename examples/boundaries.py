import argparse
import fiona
import logging
import shapely.geometry as geo
from shapely.prepared import prep

from quad_filter.filter_2d import discriminate


def make_filter(geometries, buffer_size):

    prepared_geoms = [
        prep(b.boundary) for b in geometries
    ]

    def my_filter(x0, y0, x1, y1):
        b = geo.box(x0, y0, x1, y1).buffer(buffer_size)
        return any(
            p.intersects(b) for p in prepared_geoms
        )

    return my_filter


def get_parser():
    parser = argparse.ArgumentParser("Discretisation of a boundary")
    parser.add_argument(
        "--input_url", type=str,
        help="URL of the input file (see fiona.open)",
        default="zip+https://sdf.org/~jbr/data/inverness-and-nairn.zip")
    parser.add_argument(
        "--output_path", type=str,
        help="Path of the output GeoJSON file",
        default="output.geojson")
    parser.add_argument(
        "--log-level", choices={"DEBUG", "INFO", "ERROR"},
        help="Log level", default="INFO"
    )

    return parser


def main():
    args = get_parser().parse_args()

    logging.basicConfig(
        level=args.log_level,
        datefmt='%m-%d %H:%M:%S',
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',)

    logging.info("Loading geometry")
    input_features = fiona.open(args.input_url)
    geometries = [
        geo.shape(feature["geometry"])
        for feature in input_features
    ]

    logging.info("Creating filter")
    my_filter = make_filter(geometries, 100)

    logging.info("Discriminating")
    squares_near_boundaries = list(
        discriminate(my_filter, input_features.bounds, 1000)
    )

    logging.info("Writing output")
    with fiona.open(args.output_path, "w",
                    crs=input_features.crs,
                    driver="GeoJSON",
                    schema={
                        "properties": {"near": "bool"},
                        "geometry": "Polygon",
                    }) as output:

        for near, box_coords in squares_near_boundaries:
            box = geo.box(*box_coords)
            record = {
                "properties": {"near": near},
                "geometry": geo.mapping(box)
            }
            output.write(record)

    logging.info("Done")


if __name__ == "__main__":
    main()
