import pytest
from quad_filter.filter_2d import _get_shape_parameters


@pytest.mark.parametrize(
    "width, height, cell_size, expected_size, expected_depth", [
        (10.0, 10.0, 1, 16.0, 4),
        (16.0, 10.0, 1, 16.0, 4),
        (10.0, 16.0, 1, 16.0, 4),
        (1.0, 2.0, 1, 2.0, 1),
        (2.0, 1.0, 1, 2.0, 1),
        (0.1, 0.5, 2, 2.0, 0),
        (0.1, 0.1, 1, 1.0, 0),
    ])
def test_shape_parameters(
        width, height, cell_size,
        expected_size, expected_depth):
    x0, x1 = 0, width
    y0, y1 = 0, height
    (size, depth) = _get_shape_parameters(x0, y0, x1, y1, cell_size)

    assert expected_depth == depth
    assert expected_size == size
