import pytest
import shapely.geometry as geo
from quad_filter.filter_2d import filter_rectangle


@pytest.mark.parametrize(
    "xp, yp, xa, ya, width, height, cell_size, expected_matches", [
        (12.1, 23.1, 0, 0, 100, 100, 1, 1),
        (12.1, 23.1, 10, 10, 100, 100, 1, 1),
        (12.1, 23.1, 0, 0, 100, 100, 1, 1),
        (12.0, 23.1, 0, 0, 100, 100, 1, 2),
        (12.0, 23.1, 10, 10, 100, 100, 1, 2),
        (12.0, 23.0, 0, 0, 100, 100, 1, 4),
        (12.0, 23.0, 10, 10, 100, 100, 2, 2),
        (12.0, 24.0, 10, 10, 100, 100, 2, 4),
        (-12.1, 23.1, 0, 0, 100, 100, 1, 0),
    ])
def test_get_rectangle_single(
        xp, yp, xa, ya, width, height, cell_size, expected_matches):

    def filter_function(x0, y0, x1, y1):
        return x0 <= xp <= x1 and y0 <= yp <= y1

    matches = list(
        filter_rectangle(
            filter_function,
            (xa, ya, xa + width, ya + height),
            cell_size)
    )

    assert expected_matches == len(matches)
    for x0, y0, x1, y1 in matches:
        assert x0 <= xp <= x1
        assert y0 <= yp <= y1
        assert cell_size == y1 - y0
        assert cell_size == x1 - x0


@pytest.mark.parametrize(
    "xp, yp, radius, cell_size, expected_matches", [
        (12.0, 23.0, 0.4, 1, 4),
        (12.5, 23.5, 0.4, 1, 1),
    ])
def test_get_rectangle_circle(
        xp, yp, radius, cell_size, expected_matches):

    circle = geo.Point(xp, yp).buffer(radius)

    def filter_function(x0, y0, x1, y1):
        return circle.intersects(geo.box(x0, y0, x1, y1))

    matches = list(
        filter_rectangle(
            filter_function,
            (0, 0, 100, 100),
            cell_size)
    )

    assert expected_matches == len(matches)
    for x0, y0, x1, y1 in matches:
        assert geo.box(x0, y0, x1, y1).intersects(circle)
