import mock
import pytest

from quad_filter.filter_2d import filter_rectangle


@pytest.mark.parametrize("x0, y0, x1, y1", [
    (0, 0, 10, 0),
    (0, 10, 0, 0),
    (0, 0, 10, -1),
    (0, 10, -1, 0),
])
def test_incorrect_rectangle(x0, y0, x1, y1):
    with pytest.raises(ValueError) as exc:
        list(
            filter_rectangle(
                mock.MagicMock(),
                (x0, y0, x1, y1),
                1
            )
        )
    assert "rectangle specification" in exc.value.args[0]


@pytest.mark.parametrize("cell_size", [
    0,
    -1,
])
def test_incorrect_cell_size(cell_size):
    with pytest.raises(ValueError) as exc:
        list(
            filter_rectangle(
                mock.MagicMock(),
                (0, 0, 10, 10),
                cell_size
            )
        )
    assert "cell size" in exc.value.args[0]
